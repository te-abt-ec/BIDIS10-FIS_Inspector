# FIDS BIDIS Expert GUI

## Launch
Using Inspector, launch as follows for example for PSB Gen.0:
```
/mcr/bin/inspector --panel=BIDIS10-FIS_Inspector.xml --replace=BIDIS.867.FIDSFASEC/BIDIS.361.FIDSFASEC.GEN.0
```

## Delivery
Ensure the repo is public so the CCM can retreive the xml file without being logged on. Tag the delivered version with PRO and configure the CCM as follows:
```
/mcr/bin/inspector --panel=https://gitlab.cern.ch/te-abt-ec/BIDIS10-FIS_Inspector/raw/PRO/BIDIS10-FIS_Inspector.xml --viewmode --replace=BIDIS.867.FIDSFASEC/BIDIS.361.FIDSFASEC.GEN.0
```
